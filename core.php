<?php
  $nome = isset($_POST["nome"]);
  $faixa = isset($_POST["faixaEtaria"]);
  $doenca = isset($_POST["portador"]);

  for ($i=1; $i<=6 ; $i++) {
    if ($i == 1) {
      $valor[$i] = 200;
    } else{
      $valor[$i] = $valor[$i - 1] + ($valor[$i - 1])/2;
    }
  }

  if ($doenca == "true") {
    $plus = 0.3;
  } else {
    $plus = 0;
  }

  $total = $valor[$faixa] + $valor[$faixa] * $plus
?>
<html>
	<head>
    <meta charset="UTF-8">
		<title>Exercicio - SEGURO SAUDE</title>
		<link rel="stylesheet" type="text/css" href="../css/folhaDeEstilo.css">
	</head>
	<body>
		<h1>Exercicio - SEGURO SAUDE</h1>
		<br>
    <?php echo $nome . "<br>O total do seguro saude será de: R$" . $total  ?>
	</body>
</html>
